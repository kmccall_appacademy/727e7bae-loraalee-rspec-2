def reverser(&prc)
  words = prc.call.split(" ")
  words.map { |word| word.reverse }.join(" ")
end

def adder(num=1, &prc)
  prc.call + num
end

def repeater(num=1, &prc)
  num.times { prc.call }
end
