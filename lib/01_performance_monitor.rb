def measure(num = 1, &prc)
  if num == 1
    start_time = Time.now
    prc.call
    Time.now - start_time
  else
    each_time = []
    num.times do
      start_time = Time.now
      prc.call
      each_time << Time.now - start_time
    end
    sum_times = 0
    each_time.each do |time|
      sum_times += time
    end
    sum_times / num
  end
end
